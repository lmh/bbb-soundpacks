#!/bin/bash
#
# https://codeberg.org/DigitalSouveraeneSchule/bbb/issues/3#issuecomment-185023
#

set -eu
## Define some paths
export SCRIPT=$(readlink -f "$0")
export DIR="$PWD"
export CONF="$DIR"/soundpack.conf
export CONF_DIR=$(dirname "$CONF")
export SOUNDS_DIR="$CONF_DIR"/sounds
export PLAY_SOUNDS=0

## define functions
function generate_sounds() {
    RATE="$1"
    # iterate over lines of soundpack file
    IDX=0
    while read line; do
        IDX=$(($IDX+1))
        # reading configs and cords
        CONFIG=(${line//:/ })
        CORD_CONFIG=(${CONFIG[0]//,/ })
        CORDS=(${CONFIG[1]//,/ })

        # sin [cord1] sin [cord2] ...
        SYNTH=$(printf "sin %s " "${CORDS[@]}")
        # fading options
        FADE="$(printf "%s " "${CORD_CONFIG[@]}")"

        # generating cords
        eval "sox -b 16 --rate $RATE -n -c1 output_$IDX.wav synth $SYNTH fade $FADE"
    done < "$CONF"

    # forward and backward order
    FORWARD="$(eval "printf \"output_%s.wav \" {1..$IDX}" )"
    REVERSE="$(eval "printf \"output_%s.wav \" {$IDX..1}" )"

    ## merge audio files
    eval "sox -b 16 --rate $RATE -v 1 $FORWARD $SOUNDS_DIR/$RATE/conf-unmuted.wav norm -10"
    eval "sox -b 16 --rate $RATE -v 1 $REVERSE $SOUNDS_DIR/$RATE/conf-muted.wav norm -10"

    ## cleanup tmp audio files
    eval "rm $FORWARD"
}

function generate_rate() {
    # ensure directory
    mkdir -p $SOUNDS_DIR

    # generating sounds in different rates
    RATES=( "8000" "16000" "32000" "48000" )

    for RATE in "${RATES[@]}"
    do
        mkdir -p "$SOUNDS_DIR"/$RATE
        generate_sounds $RATE
    done
}

function print_infos() {
    if [ ! -f "$CONF" ]; then
        echo -e "Config file not found: $CONF"
        exit 1
    fi
    echo -e "Sound config file: $CONF"
    echo -e "Exporting sounds to: $SOUNDS_DIR"
}

## parsing arguments
#Usage print
usage() {
    echo "Usage: $0 [flag]" >&2
    echo "
    Flags:
   -f [file],   generate sound from configfile [file]
   -e [path],   exporting sounds to [path]/sounds
   ">&2
    exit 1
}

while getopts ':f:e:p' opt; do
case "$opt" in
   'f')export CONF="$OPTARG";
       export CONF_DIR=$(dirname "$CONF");
       export SOUNDS_DIR="$CONF_DIR"/sounds;
       ;;
   'e')export SOUNDS_DIR="$OPTARG"/sounds;
       ;;
   'p')export PLAY_SOUNDS=1;
       ;;
    *) usage;
       ;;
esac
done

# generating sound files
print_infos && generate_rate

# playing sounds
if [ $PLAY_SOUNDS -eq 1 ]; then
play "$SOUNDS_DIR"/48000/conf-unmuted.wav
sleep 1
play "$SOUNDS_DIR"/48000/conf-muted.wav
fi