# Soundpacks for BBB

A repository to collect some BBB sounds

##### Requirements

* python3
* ffmpeg
* sox

##### Converting raw sound files

In a subfolder of `soundpacks` is a folder located called `raw`. Within this folder the two raw soundfiles `conf-muted.wav` and `conf-unmuted.wav` are stored.

By running `convert.py` sounds with different bitrates will be generated inside the subfolders `sounds`.

```bash
./convert.py
```

