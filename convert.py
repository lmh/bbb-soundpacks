#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Covnerting sound files into  different qualities
"""
import os
import glob2

bitrates=  [
            '8000',
            '16000',
            '32000',
            '48000',
            ]

extension = 'wav'

rawfiles = []
for ext in ('WAV','wav','MP3','mp3','FLAC','flac'):
   rawfiles.extend(glob2.glob(os.getcwd()+"/**/*.{:}".format(ext)))
rawfiles = [k for k in glob2.glob(os.getcwd()+"/**/*.wav") if '/raw/' in k]

for rawfile in rawfiles:
    basename = os.path.splitext(os.path.basename(rawfile))[0]
    soundsdir = os.path.abspath(rawfile+'/../../sounds/')
    for bitrate in bitrates:
        bitratepath = os.path.join(soundsdir,bitrate)
        
        if not os.path.exists(bitratepath):
            os.makedirs(bitratepath)
            
        cmd =   [
                'ffmpeg',
                '-y',
                '-i',
                rawfile,
                '-ar',
                bitrate,
                os.path.join(bitratepath,basename+'.'+extension),
                ]

        os.system(" ".join(cmd))
