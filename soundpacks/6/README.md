# Soundpack 6

generating with `sox`

```bash
./mksound.sh --overwrite
```

From:

https://codeberg.org/DigitalSouveraeneSchule/bbb/raw/commit/ceb12726c4349680d05ba7e89ed893ca74c4041e/helper-scripts/mksound.sh

