#!/bin/sh
#
# https://codeberg.org/DigitalSouveraeneSchule/bbb/raw/commit/ceb12726c4349680d05ba7e89ed893ca74c4041e/helper-scripts/mksound.sh
#

set -eu
## Define some paths
export SCRIPT=$(readlink -f "$0")
export DIR="$PWD"
export SOUNDS_DIR="$DIR"/sounds
export SOX_OR_PLAY="sox"

export FREQ_lower=50
export FREQ_upper=120
export DURATION=0.8
export FADE=0.2

## define functions

generate_sounds() {
    RATE="$1"
    FREQ_lower="$2"
    FREQ_upper="$3"
    DURATION="$4"
    FADE="$5"
    NORM="-5"

    ## generate audio files on specific rate
    if [ "$SOX_OR_PLAY" = "play" ]; then
        $SOX_OR_PLAY -b 16 --rate $RATE -n -c1 synth $DURATION sin $FREQ_lower+$FREQ_upper fade $FADE $DURATION $FADE norm $NORM
        sleep 0.5
        $SOX_OR_PLAY -b 16 --rate $RATE -n -c1 synth $DURATION sin $FREQ_upper+$FREQ_lower fade $FADE $DURATION $FADE norm $NORM
    else
        mkdir -p "$SOUNDS_DIR"/$RATE
        $SOX_OR_PLAY -b 16 --rate $RATE -n -c1 $SOUNDS_DIR/$RATE/conf-unmuted.wav synth $DURATION sin $FREQ_lower+$FREQ_upper fade $FADE $DURATION $FADE norm $NORM
        $SOX_OR_PLAY -b 16 --rate $RATE -n -c1 $SOUNDS_DIR/$RATE/conf-muted.wav synth $DURATION sin $FREQ_upper+$FREQ_lower fade $FADE $DURATION $FADE norm $NORM
    fi
}

generate_rate() {
    # ensure directory
    mkdir -p $SOUNDS_DIR

    ## play or generate audio files
    if [ "$SOX_OR_PLAY" = "play" ]; then
        generate_sounds 48000 $FREQ_lower $FREQ_upper $DURATION $FADE
    else
        # generating sounds in different rates
        for RATE in 8000 16000 32000 48000 ; do
            generate_sounds $RATE $FREQ_lower $FREQ_upper $DURATION $FADE
        done
    fi
}

## parsing arguments
#Usage print
usage() {
    echo "Usage: $0 --frequency [lower] [upper] --duation [duration] --fade [fade] [flag]" >&2
    echo "
    flags:
      --overwrite,                  generate sounds and overwrite existing sounds
      --play,                       play sound
   ">&2
    exit 1
}

if [ $# -eq 0 ]; then
    usage
fi

while [ $# -gt 0 ] ; do
  case $1 in
    -f | --frequency | frequency)       export FREQ_lower="$2"; export FREQ_upper="$3" ;;
    -d | --duration | duration)         export DURATION="$2" ;;
    --fade | fade)                      export FADE="$2" ;;
    -o | --overwrite | overwrite)       generate_rate ;;
    -p | --play | play)                 SOX_OR_PLAY="play" generate_rate ;;
  esac
  shift
done

