# Soundpack 5

generating with `sox` and play

```bash
../../generate/mksound.sh -p
```

From:

[https://codeberg.org/DigitalSouveraeneSchule/bbb/issues/3#issuecomment-185023](https://codeberg.org/DigitalSouveraeneSchule/bbb/issues/3#issuecomment-185023)